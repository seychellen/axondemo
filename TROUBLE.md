## Bug
Failed to configure a DataSource: 'url' attribute is not specified and no embedded datasource could be configured

pom.xml:
    <dependency>
        <groupId>com.h2database</groupId>
        <artifactId>h2</artifactId>
    </dependency>

application.properties:
    spring.datasource.driver-class-name=org.h2.Driver

