package de.kotlincook.axondemo.readmodel

import de.kotlincook.axondemo.coreapi.FindFoodCartQuery
import de.kotlincook.axondemo.coreapi.FoodCartCreatedEvent
import de.kotlincook.axondemo.coreapi.ProductDeselectedEvent
import de.kotlincook.axondemo.coreapi.ProductSelectedEvent
import org.axonframework.eventhandling.EventHandler
import org.axonframework.queryhandling.QueryHandler
import org.springframework.stereotype.Component


@Component
internal class FoodCartProjector(private val repository: FoodCartViewRepository) {

    @EventHandler
    fun on(event: FoodCartCreatedEvent) {
        val foodCartView = FoodCartView(event.foodCartId, mutableMapOf())
        repository.save(foodCartView)
    }

    @EventHandler
    fun on(event: ProductSelectedEvent) {
        repository.findById(event.foodCartId).ifPresent { foodCartView: FoodCartView ->
            foodCartView.addProducts(
                event.productId,
                event.quantity
            )
        }
    }

    @EventHandler
    fun on(event: ProductDeselectedEvent) {
        repository.findById(event.foodCartId).ifPresent { foodCartView: FoodCartView ->
            foodCartView.removeProducts(
                event.productId,
                event.quantity
            )
        }
    }

    @QueryHandler
    fun handle(query: FindFoodCartQuery): FoodCartView? {
        return repository.findById(query.foodCartId).orElse(null)
    }
}