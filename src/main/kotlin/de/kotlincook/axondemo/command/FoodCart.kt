package de.kotlincook.axondemo.command

import de.kotlincook.axondemo.coreapi.*
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.modelling.command.AggregateLifecycle
import org.axonframework.spring.stereotype.Aggregate
import java.util.*


@Aggregate
class FoodCart {

    @AggregateIdentifier
    lateinit var foodCartId: UUID
    var confirmed: Boolean = false
    lateinit var selectedProducts: HashMap<UUID, Int>

    // Wird von Axon benötigt
    constructor()

    @CommandHandler
    constructor(command: CreateFoodCartCommand) {
        AggregateLifecycle.apply(FoodCartCreatedEvent(command.foodCartId))
    }

    @CommandHandler
    fun handle(command: SelectProductCommand) {
        AggregateLifecycle.apply(ProductSelectedEvent(
            foodCartId, command.productId, command.quantity))
    }

    @CommandHandler
    fun handle(command: DeselectProductCommand) {
        if (command.productId !in selectedProducts) {
            throw ProductDeselectionException(
                "Cannot deselect a product which has not been selected for this Food Cart")
        }
        if (selectedProducts[command.productId]!! - command.quantity < 0) {
            throw ProductDeselectionException(
                    "Cannot deselect more products of ID [$command.productId] than have been selected initially")
        }
        AggregateLifecycle.apply(ProductDeselectedEvent(
            foodCartId, command.productId, command.quantity))
    }

    @EventSourcingHandler
    fun on(event: FoodCartCreatedEvent) {
        foodCartId = event.foodCartId
        selectedProducts = HashMap()
        confirmed = false
    }

    @EventSourcingHandler
    fun on(event: ProductSelectedEvent) {
        selectedProducts.merge(event.productId, event.quantity, Integer::sum)
    }

    @EventSourcingHandler
    fun on(event: ProductDeselectedEvent) {
        selectedProducts.computeIfPresent(event.productId) {
                productId, quantity -> quantity - event.quantity
        }
    }

    @EventSourcingHandler
    fun on(event: OrderConfirmedEvent?) {
        confirmed = true
    }

}