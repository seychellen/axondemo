package de.kotlincook.axondemo.coreapi

import java.util.*

data class FindFoodCartQuery(val foodCartId: UUID)

class RetrieveProductOptionsQuery