package de.kotlincook.axondemo.coreapi

class ProductDeselectionException(s: String) : Exception(s)
