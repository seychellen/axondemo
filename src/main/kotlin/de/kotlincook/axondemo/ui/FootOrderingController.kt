package de.kotlincook.axondemo.ui

import de.kotlincook.axondemo.coreapi.CreateFoodCartCommand
import de.kotlincook.axondemo.coreapi.DeselectProductCommand
import de.kotlincook.axondemo.coreapi.FindFoodCartQuery
import de.kotlincook.axondemo.coreapi.SelectProductCommand
import de.kotlincook.axondemo.readmodel.FoodCartView
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.messaging.responsetypes.ResponseTypes
import org.axonframework.queryhandling.QueryGateway
import org.springframework.web.bind.annotation.*
import java.util.*
import java.util.concurrent.CompletableFuture


@RequestMapping("/foodCart")
@RestController
internal class FoodOrderingController(
    val commandGateway: CommandGateway,
    val queryGateway: QueryGateway
) {

    @PostMapping("/create/{foodCartId}")
    fun createFoodCart(@PathVariable foodCartId: String): CompletableFuture<UUID> {
        val completableFuture: CompletableFuture<UUID> =
            commandGateway.send(CreateFoodCartCommand(UUID.fromString(foodCartId)))
        return completableFuture
    }

    @PostMapping("/create")
    fun createFoodCart(): CompletableFuture<UUID> {
        val completableFuture: CompletableFuture<UUID> =
            commandGateway.send(CreateFoodCartCommand(UUID.randomUUID()))
        return completableFuture
    }

    @PostMapping("/{foodCartId}/select/{productId}/quantity/{quantity}")
    fun selectProduct(
        @PathVariable foodCartId: String,
        @PathVariable productId: String,
        @PathVariable quantity: Int
    ) {
        commandGateway.send<Any>(
            SelectProductCommand(UUID.fromString(foodCartId), UUID.fromString(productId), quantity)
        )
    }

    @PostMapping("/{foodCartId}/deselect/{productId}/quantity/{quantity}")
    fun deselectProduct(
        @PathVariable foodCartId: String,
        @PathVariable productId: String,
        @PathVariable quantity: Int
    ) {
        commandGateway.send<Any>(
            DeselectProductCommand(UUID.fromString(foodCartId), UUID.fromString(productId), quantity)
        )
    }

    @GetMapping("/{foodCartId}")
    fun findFoodCart(@PathVariable foodCartId: String): CompletableFuture<FoodCartView> {
        return queryGateway.query(
            FindFoodCartQuery(UUID.fromString(foodCartId)),
            ResponseTypes.instanceOf(FoodCartView::class.java)
        )
    }
}